#!/bin/bash
#
# Postgres Data Directory must be initialised for this script to initialise
echo "**************************** HERE WE GO! **************************"

# entrypoint.sh
# Docker entrypoint script.
# Wait until Postgres is ready
# if fails first time, ensure that you clear out POSTGRES_DATA directory, i.e. it should be empty to commence 
while ! pg_isready -q -h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USER
do
  echo "$(date) - ****** waiting for database to start ******"
  sleep 2
done

# Create, migrate, and seed database if it doesn't exist.
if [[ -z `psql -Atqc "\\list $POSTGRES_DB"` ]]; then
  echo "Database $POSTGRES_DB does not exist. Creating..."
  createdb -E UTF8 $POSTGRES_DB -l en_US.UTF-8 -T template0
  mix ecto.migrate
  mix run priv/repo/seeds.exs
  echo "****** Database: $POSTGRES_DB created. ******"
else
  echo "****** ${POSTGRES_DB} exists ******"
fi

exec mix phx.server

