# LOGIN TO GITLAB CONTAINER REGISTRY
login:
	@docker login \
		registry.gitlab.com
# RUN A DOCKER BUILD AND TAG THE IMAGE TO ENABLE PUSHING TO THE REGISTRY
build:
	@docker build \
		-t registry.gitlab.com/username/app_name .
# bring the docker environment up
up:
	@docker-compose up -d phoenix postgres webpack
# Run the container interactively
phoenix-run:
	@docker-compose run --rm --name app_name_phoenix phoenix /bin/bash
# PUSH THE CONTAINER TO THE REGISTRY: https://gitlab.com/username/ihs/container_registry
push:
	@docker push registry.gitlab.com/username/app_name
# RUN A BUILD AND PUSH OPERATION
bp:
	build push
