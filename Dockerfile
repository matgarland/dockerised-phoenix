# Phoenix service Dockerfile
#
# Installations & Versions:
#
# Erlang OTP: 21.2.6
# Elixir: 1.8.1
# Phoenix: 1.4.0
#
FROM elixir:1.8.1
# from Elixir 1.8.1 we get erlang v.21.2.6
LABEL maintainer="Mat Garland <matg74@gmail.com>"
# ensure that terminal commandds are run with  '-y' as default
ENV DEBIAN_FRONTEND=noninteractive
# Phoenix's app directory
ENV PUB_DIR=/app

# COPY DIRS TO THE CONTAINER TO ENABLE TWO WAY APP GENERATION WITH mix phx.new
RUN mkdir -p ${PUB_DIR}

# Copy code base into container
COPY . ${PUB_DIR}

# switch to the /app directory
WORKDIR ${PUB_DIR}

# libssl is required for a Phoenix release to run somewhere other than our workstation
# postgresql-client package provides the psql command used by Ecto's Postgres adapter,
RUN apt update && \
    apt install -y curl \
    inotify-tools \
    postgresql-client && \
    apt-get autoclean

# INSTALL node
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt install -y nodejs

# Install hex package manager
# By using --force, we don’t need to type “Y” to confirm the installation
RUN mix local.hex --force
# Compile the app
RUN mix do compile

CMD ["/app/entrypoint.sh"]
