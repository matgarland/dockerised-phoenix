# dockerised-phoenix  

### uri: https://gitlab.com/matgarland/dockerised-phoenix  

### Dockerised Elixir Phoenix Project Files  

#### Erlang OTP: 21.2.6  
#### Elixir: 1.8.1  
#### Phoenix: 1.4.0  
#### Node 10.12  

## STEPS TO USE dockerised_phoenix IN YOUR PROJECT  

1. `mkdir app_name`
2. `chown -R ${USER}:${USER} app_name`
3. From the app_name parent dir: `mix phx.new app_name`
4. Edit the docker related files, and Makefile, replacing app_name with your project's name, and username with your username
5. Install dependencies with `mix deps.get`
6. Install Node.js dependencies with `cd assets && npm install`
7. Run `docker-compose -f docker-compose.yml build postgres webpack phoenix` and ensure database is operational before running step #8
8. Create and migrate your database with `mix ecto.create`
9. Access from http://localhost:4000

